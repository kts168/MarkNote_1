<?php

	require_once(WEBROOT.'/include/note.php');
	
	$noteid=isset($_GET['id'])?(int)$_GET['id']:0;
	$user=isset($_GET['user'])?trim($_GET['user']):'';
	if($noteid){
		//判断noteid和用户是否匹配
		$noteuser=getNoteUser($noteid);
		if(!$noteuser or $noteuser<>$user) exit('no user');
		//获取配置
		$ext=json_decode(getNoteSettings($noteid));
		if(isset($ext->share)&& $ext->share==1){
			//获取内容
			$content=getNote($noteid);
			$content = str_replace(array("<",">"), array("&lt;","&gt;"), $content);//避免html解析
			$js='';
			//[code]格式支持
			$codestatus=preg_match('/\s{4}\[\w+\]/',$content)?1:0;
			$js.='<script type="text/javascript" charset="utf-8">var CODESTATUS='.$codestatus.';</script>';
			//数学公式的支持
			if(preg_match('/\$\$.+\\\\.+\$\$/',$content) or preg_match('/\$.+\\\\.+\$/',$content) or preg_match('/\\\\\\\\\(.+\\\\.+\\\\\\\\\)/',$content) or preg_match('/\\\\\\\\\[.+\\\\.+\\\\\\\\\]/',$content)){
				$js.='<script src="https://cdn.bootcss.com/mathjax/2.7.4/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>';
$js.=<<<heredoc

<script type="text/x-mathjax-config">
MathJax.Hub.Config({
    tex2jax: {
        inlineMath: [ ['$','$'], ["\\\\(","\\\\)"] ],
        displayMath: [ ['$$','$$'], ["\\\\[","\\\\]"] ]
    }
});
</script>
heredoc;
			}

			include(template('show'));
		}
		

	}
	
?>
