<?php
require_once('common.php');
require_once 'include/user.php';

if(isset($_POST['type'])){
	switch ($_POST['type']) {
		case 'login':
			login($_POST['username'], $_POST['passwd']);
			break;
		case 'register':
			//register($_POST['username'], $_POST['email'], $_POST['passwd'], $_POST['nickname']);
			break;
		case 'logout':
			logout();
			break;
		default:
			# code...
			break;
	}
}

if(hasLogin()){
		header("location: ./"); 
}else{
	if(isset($_GET['t']) && $_GET['t'] == 'register'){
		include(template('register'));
	}else{
		include(template('login'));	
	}
}