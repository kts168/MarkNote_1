<?php
	require_once('common.php');
	require_once WEBROOT.'/include/user.php';
	require_once WEBROOT.'/include/note.php';

	if(!hasLogin()){
		echo 'Please login';
		exit();
	}
	//api
	if( hasLogin() && isset($_POST['action']) && $_POST['action'] == 'getNotelist' ){
		$theNotebooks = getUserNotebooks($USERNAME);
		//var_dump($theNotebooks);
		include(template('tag-notebooks'));
		exit();
	}
$settings=getUsersetting($USERNAME);
$js='';
if($settings->math) $js='<script src="https://cdn.bootcss.com/mathjax/2.7.4/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>';
$js.=<<<heredoc
<script type="text/javascript" charset="utf-8">
	var NOTEID=$settings->noteid,MATHSTATUS=$settings->math,WAITINTERVAL=$settings->interval,CODESTATUS=$settings->code;
</script>
<script type="text/x-mathjax-config">
MathJax.Hub.Config({
    tex2jax: {
        inlineMath: [ ['$','$'], ["\\\\(","\\\\)"] ],
        displayMath: [ ['$$','$$'], ["\\\\[","\\\\]"] ]
    }
});
</script>
heredoc;
$css=$settings->black?'github-markdown':'black-markdown';
include(template('edit'));