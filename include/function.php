<?php

//模板函数
function template($template = 'index', $dir = '') {

	$to = WEBROOT.'/cache/tpl/'.($dir ? $dir.'/' : '').$template.'.php';
	$isfileto = is_file($to);
	if($dir) $dir = $dir.'/';
    $from = WEBROOT.'/tpl/'.$dir.$template.'.html';
    if(!$isfileto || filemtime($from) > filemtime($to) || (filesize($to) < 52 && filesize($from) > 0)) {
		template_compile($from, $to);
	}
	return $to;
}

function template_compile($from, $to) {
    $content = template_parse(@file_get_contents($from));
    @file_put_contents($to, $content);
}



function template_parse($str) {
	$str = preg_replace("/\<\!\-\-\[(.+?)\]\-\-\>/", "", $str);
	$str = preg_replace("/\<\!\-\-\{(.+?)\}\-\-\>/s", "{\\1}", $str);
	$str = preg_replace("/\{template\s+([^\}]+)\}/", "<?php include template(\\1);?>", $str);
	$str = preg_replace("/\{php\s+(.+)\}/", "<?php \\1?>", $str);
	$str = preg_replace("/\{if\s+(.+?)\}/", "<?php if(\\1) { ?>", $str);
	$str = preg_replace("/\{else\}/", "<?php } else { ?>", $str);
	$str = preg_replace("/\{elseif\s+(.+?)\}/", "<?php } else if(\\1) { ?>", $str);
	$str = preg_replace("/\{\/if\}/", "<?php } ?>\r\n", $str);
	$str = preg_replace("/\{loop\s+(\S+)\s+(\S+)\}/", "<?php if(is_array(\\1)) { foreach(\\1 as \\2) { ?>", $str);
	$str = preg_replace("/\{loop\s+(\S+)\s+(\S+)\s+(\S+)\}/", "<?php if(is_array(\\1)) { foreach(\\1 as \\2 => \\3) { ?>", $str);
	$str = preg_replace("/\{\/loop\}/", "<?php } } ?>", $str);
	$str = preg_replace("/\{([a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*\(([^{}]*)\))\}/", "<?php echo \\1;?>", $str);
	$str = preg_replace_callback("/<\?php([^\?]+)\?>/s", "template_addquote1", $str);
	$str = preg_replace("/\{(\\$[a-zA-Z_\x7f-\xff][\->a-zA-Z0-9_\+\-\x7f-\xff]*)\}/", "<?php echo \\1;?>", $str);
	$str = preg_replace_callback("/\{(\\$[a-zA-Z0-9_\[\]\'\"\$\+\-\x7f-\xff]+)\}/s", "template_addquote2", $str);
	$str = preg_replace("/\{([A-Z_\x7f-\xff][A-Z0-9_\x7f-\xff]*)\}/s", "<?php echo \\1;?>", $str);
	$str = preg_replace("/\'([A-Za-z]+)\[\'([A-Za-z\.]+)\'\](.?)\'/s", "'\\1[\\2]\\3'", $str);
	$str = preg_replace("/(\r?\n)\\1+/", "\\1", $str);
	$str = str_replace("\t", '', $str);
	$str = "<?php defined('APPNAME') or exit('Access Denied');?>".trim($str);
	return $str;
}

function template_addquote1($matches) {
	return str_replace("\\\"", "\"", preg_replace("/\[([a-zA-Z0-9_\-\.\x7f-\xff]+)\]/s", "['\\1']", $matches[0]));
}

function template_addquote2($matches) {
	return '<?php echo '.str_replace("\\\"", "\"", preg_replace("/\[([a-zA-Z0-9_\-\.\x7f-\xff]+)\]/s", "['\\1']", $matches[1])).';?>';
}


//安全函数

function dsafe($string, $type = 1) {
	if(is_array($string)) {
		return array_map('dsafe', $string);
	} else {
		if($type) {
			$string = str_replace('<em></em>', '', $string);
			$string = preg_replace("/\<\!\-\-([\s\S]*?)\-\-\>/", "", $string);
			$string = preg_replace("/\/\*([\s\S]*?)\*\//", "", $string);
			$string = preg_replace("/&#([a-z0-9]{1,})/i", "<em></em>&#\\1", $string);
			$match = array("/s[\s]*c[\s]*r[\s]*i[\s]*p[\s]*t/i","/d[\s]*a[\s]*t[\s]*a[\s]*\:/i","/b[\s]*a[\s]*s[\s]*e/i","/e[\\\]*x[\\\]*p[\\\]*r[\\\]*e[\\\]*s[\\\]*s[\\\]*i[\\\]*o[\\\]*n/i","/i[\\\]*m[\\\]*p[\\\]*o[\\\]*r[\\\]*t/i","/on([a-z]{2,})([\(|\=|\s]+)/i","/about/i","/frame/i","/link/i","/meta/i","/textarea/i","/eval/i","/alert/i","/confirm/i","/prompt/i","/cookie/i","/document/i","/newline/i","/colon/i","/<style/i","/\\\x/i");
			$replace = array("s<em></em>cript","da<em></em>ta:","ba<em></em>se","ex<em></em>pression","im<em></em>port","o<em></em>n\\1\\2","a<em></em>bout","f<em></em>rame","l<em></em>ink","me<em></em>ta","text<em></em>area","e<em></em>val","a<em></em>lert","/con<em></em>firm/i","prom<em></em>pt","coo<em></em>kie","docu<em></em>ment","new<em></em>line","co<em></em>lon","<sty1e","\<em></em>x");
			return str_replace(array('isShowa<em></em>bout', 'co<em></em>ntrols'), array('isShowAbout', 'controls'), preg_replace($match, $replace, $string));
		} else {
			return str_replace(array('<em></em>', '<sty1e'), array('', '<style'), $string);
		}
	}
}

function strip_sql($string, $type = 1) {
	if(is_array($string)) {
		return array_map('strip_sql', $string);
	} else {
		if($type) {
			$string = preg_replace("/\/\*([\s\S]*?)\*\//", "", $string);
			$string = preg_replace("/0x([a-f0-9]{2,})/i", '0&#120;\\1', $string);
			$string = preg_replace_callback("/(select|update|replace|delete|drop)([\s\S]*?)(from)/i", 'strip_wd', $string);
			$string = preg_replace_callback("/(load_file|substring|substr|reverse|trim|space|left|right|mid|lpad|concat|concat_ws|make_set|ascii|bin|oct|hex|ord|char|conv)([^a-z]?)\(/i", 'strip_wd', $string);
			$string = preg_replace_callback("/(union|where|having|outfile|dumpfile)/i", 'strip_wd', $string);
			return $string;
		} else {
			return str_replace(array('&#95;','&#100;','&#101;','&#103;','&#105;','&#109;','&#110;','&#112;','&#114;','&#115;','&#116;','&#118;','&#120;'), array('_','d','e','g','i','m','n','p','r','s','t','v','x'), $string);
		}
	}
}

function strip_wd($m) {
	if(is_array($m) && isset($m[1])) {
		$wd = substr($m[1], 0, -1).'&#'.ord(strtolower(substr($m[1], -1))).';';
		if(isset($m[3])) return $wd.$m[2].$m[3];
		if(isset($m[2])) return $wd.$m[2].'(';
		return $wd;
	}
	return '';
}
